import { Component } from '@angular/core';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { PluginCallPlugin } from 'plugincall';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(private broadcaster: Broadcaster) {


// Listen to events from Native
this.broadcaster.addEventListener('eventName').subscribe((call) => console.log(call + "teste"));

  }
  
}